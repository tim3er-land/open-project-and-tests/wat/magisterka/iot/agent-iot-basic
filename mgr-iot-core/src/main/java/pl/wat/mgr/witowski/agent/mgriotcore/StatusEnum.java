package pl.wat.mgr.witowski.agent.mgriotcore;

public enum StatusEnum {
    UP("UP"), DOWN("DOWN");

    String status;

    private StatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
