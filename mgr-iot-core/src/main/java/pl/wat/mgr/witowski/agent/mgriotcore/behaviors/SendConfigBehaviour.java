package pl.wat.mgr.witowski.agent.mgriotcore.behaviors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import pl.wat.mgr.witowski.agent.mgriotcore.SecurityUtils;
import pl.wat.mgr.witowski.agent.mgriotcore.agent.AbstrackAgent;
import pl.wat.mgr.witowski.agent.mgriotcore.dto.AgentConfigDto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;
import static pl.wat.mgr.witowski.agent.mgriotcore.SecurityUtils.getPrivateKey;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class SendConfigBehaviour extends BasicBehavior {
    private static final Logger LOGGER = Logger.getLogger(SendConfigBehaviour.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(SendConfigBehaviour.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    private List<String> topics;

    private String status;

    public SendConfigBehaviour(Agent a, String agentUid, List<String> topics, String status) {
        super(a, agentUid);
        this.topics = topics;
        this.status = status;
    }

    @Override
    protected void sendMessage(String messageUid) throws Exception {
        AgentConfigDto agentConfigDto = new AgentConfigDto(LocalDateTime.now().toString(), messageUid, topics, agentUid, status);
        ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.addReceiver(new AID("hub", AID.ISLOCALNAME));
        msg.setLanguage("CONFIG");
        msg.setContent(prepareAgentMsgContent(agentConfigDto));
        myAgent.send(msg);
    }

    private String prepareAgentMsgContent(AgentConfigDto agentRawDataDto) throws Exception {
        String encrypt = SecurityUtils.encrypt(objectMapper.writeValueAsString(agentRawDataDto), Base64.getDecoder().decode(pubDataDecript));
//        String encrypt = SecurityUtils.encodeStringToBase64(objectMapper.writeValueAsString(agentRawDataDto));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(agentUid);
        stringBuilder.append(".");
        stringBuilder.append(encrypt);
        stringBuilder.append(".");
        stringBuilder.append(SecurityUtils.sign(encrypt, getPrivateKey(privAgentSign)));
        return stringBuilder.toString();
    }
}
