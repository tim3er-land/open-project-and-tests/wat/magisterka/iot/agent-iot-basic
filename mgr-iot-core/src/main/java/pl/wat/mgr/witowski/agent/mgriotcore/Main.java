package pl.wat.mgr.witowski.agent.mgriotcore;

import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.SendDataBehaviour;

import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String args[]) {
        new SendDataBehaviour(null, "", "", new HashMap<>()).action();
    }
}
