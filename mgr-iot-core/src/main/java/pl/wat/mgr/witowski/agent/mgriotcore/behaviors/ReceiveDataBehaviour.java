package pl.wat.mgr.witowski.agent.mgriotcore.behaviors;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import org.bouncycastle.util.encoders.Base64;
import pl.wat.mgr.witowski.agent.mgriotcore.SecurityUtils;
import pl.wat.mgr.witowski.agent.mgriotcore.agent.AbstrackAgent;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static pl.wat.mgr.witowski.agent.mgriotcore.SecurityUtils.verify;

/**
 * @author: Piotr Witowski
 * @date: 02.12.2021
 * @project simple-jade-projectv2
 * Day of week: czwartek
 */
public class ReceiveDataBehaviour extends TickerBehaviour {
    private static final Logger LOGGER = Logger.getLogger(ReceiveDataBehaviour.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(ReceiveDataBehaviour.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    private AbstrackAgent agent;
    private boolean verifyData;
    protected String pubDataVerify;
    protected String decryptAgentPrivBytes;

    public ReceiveDataBehaviour(AbstrackAgent a, long period) {
        super(a, period);
        this.agent = a;
        this.verifyData = true;
        try {
            this.pubDataVerify = Files.readString(Paths.get("secrets/secretSigh.key"), StandardCharsets.US_ASCII);
            this.decryptAgentPrivBytes = Files.readString(Paths.get("encrypt/private.key"), StandardCharsets.US_ASCII);
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Error occured get keys", ex);
        }
    }

    public ReceiveDataBehaviour(Agent a, long period, AbstrackAgent agent, boolean verifyData) {
        super(a, period);
        this.agent = agent;
        this.verifyData = verifyData;
    }

    @Override
    protected void onTick() {
        ACLMessage msg = myAgent.receive();
        try {
            if (msg != null && msg.getContent() != null && !msg.getContent().isEmpty()) {
                String message = msg.getContent();
                String[] splitMessage = message.split("\\.");
                if (splitMessage.length != 3) {
                    LOGGER.log(Level.SEVERE, "message not contain 3 parts");
                }
                boolean verify = verify(pubDataVerify, splitMessage[1], splitMessage[2]);
                if (verifyData && !verify) {
                    LOGGER.log(Level.WARNING, "Message not come from trust sources");
                } else {
                    LOGGER.log(Level.FINE, "Message come from trust sources");
                    String payloadDecrypt = SecurityUtils.decrypt(splitMessage[1], Base64.decode(decryptAgentPrivBytes));
                    agent.onMessage(payloadDecrypt);
                }
            } else {
                block();
            }
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Error occured processed message", ex);
            block();
        }
    }
}
