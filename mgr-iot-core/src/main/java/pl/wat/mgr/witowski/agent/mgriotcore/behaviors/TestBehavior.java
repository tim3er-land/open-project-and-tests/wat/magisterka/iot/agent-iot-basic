package pl.wat.mgr.witowski.agent.mgriotcore.behaviors;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.StatusEnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class TestBehavior extends TickerBehaviour {
    private String agentUid;
    private boolean testData;

    public TestBehavior(Agent a, long period, String agentUid, boolean testData) {
        super(a, period);
        this.agentUid = agentUid;
        this.testData = testData;
    }

    @Override
    protected void onTick() {
        if (testData) {
            HashMap<String, Object> objectObjectHashMap = new HashMap<>();
            objectObjectHashMap.put("test", new Random().nextDouble() * 10);
            objectObjectHashMap.put("test1", "test");
            objectObjectHashMap.put("test2", "test");
            objectObjectHashMap.put("test3", "test");
            objectObjectHashMap.put("test4", "test");
            objectObjectHashMap.put("test5", "test");
            objectObjectHashMap.put("test6", "test");
            objectObjectHashMap.put("test7", "test");
            objectObjectHashMap.put("test9", "test");
            objectObjectHashMap.put("test10", "test");
            myAgent.addBehaviour(new SendDataBehaviour(myAgent, agentUid, "/test", objectObjectHashMap));
        } else {
            myAgent.addBehaviour(new SendConfigBehaviour(myAgent, agentUid, Arrays.asList("/test", "/test1", "/test2"), StatusEnum.UP.getStatus()));
        }
    }
}
