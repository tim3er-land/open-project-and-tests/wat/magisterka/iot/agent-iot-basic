package pl.wat.mgr.witowski.agent.mgriotcore.agent;

import jade.core.Agent;
import jade.tools.logging.LogManager;
import lombok.extern.log4j.Log4j2;
import pl.wat.mgr.witowski.agent.mgriotcore.StatusEnum;
import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.ReceiveDataBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.SendConfigBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.SendDataBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.dto.TopicConfig;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author: Piotr Witowski
 * @date: 04.12.2021
 * @project simple-jade-projectv2
 * Day of week: sobota
 */
public abstract class AbstrackAgent extends Agent {

    private static final Logger LOGGER = Logger.getLogger(AbstrackAgent.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(AbstrackAgent.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    protected String agentUid;
    List<String> topicsConfig;

    public AbstrackAgent(String agentUid, List<String> topicsConfig) {
        this.agentUid = agentUid;
        this.topicsConfig = topicsConfig;
    }

    @Override
    protected void setup() {
        if (topicsConfig != null && !topicsConfig.isEmpty()) {
            LOGGER.info("start setup agent with uid " + agentUid);
            addBehaviour(new SendConfigBehaviour(this, agentUid, topicsConfig, StatusEnum.UP.getStatus()));
            addBehaviour(new ReceiveDataBehaviour(this, 100));
        }
        LOGGER.info("start setup agent with uid " + agentUid);
        super.setup();
    }

    public void onMessage(String message) {
        LOGGER.log(Level.FINE, message);
    }


    @Override
    protected void takeDown() {
        LOGGER.info("END setup agent with uid " + agentUid);
        addBehaviour(new SendConfigBehaviour(this, agentUid, Collections.emptyList(), StatusEnum.DOWN.getStatus()));
    }


}
