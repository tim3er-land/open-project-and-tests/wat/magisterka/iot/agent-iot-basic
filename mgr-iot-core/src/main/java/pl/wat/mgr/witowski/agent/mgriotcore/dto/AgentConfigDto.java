package pl.wat.mgr.witowski.agent.mgriotcore.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentConfigDto {
    private String creationDate;
    private String messageUid;
    private List<String> topic;
    private String agentUid;
    private String status;
}
