package pl.wat.mgr.witowski.agent.mgriotcore.behaviors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import pl.wat.mgr.witowski.agent.mgriotcore.agent.AbstrackAgent;
import pl.wat.mgr.witowski.agent.mgriotcore.dto.AgentConfigDto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.UUID;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;

public class BasicBehavior extends OneShotBehaviour {
    private static final Logger LOGGER = Logger.getLogger(BasicBehavior.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
        Logger root = Logger.getLogger(BasicBehavior.class.getName());
        root.setLevel(Level.ALL);
        for (Handler handler : root.getHandlers()) {
            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
                handler.setLevel(Level.ALL);
        }
    }

    protected String agentUid;
    protected ObjectMapper objectMapper;
    protected String pubDataDecript;
    protected String privAgentSign;


    public BasicBehavior(Agent a, String agentUid) {
        super(a);
        this.agentUid = agentUid;
        try {
            this.pubDataDecript = Files.readString(Paths.get("secrets/secretDecrypt.key"), StandardCharsets.US_ASCII);
            this.privAgentSign = Files.readString(Paths.get("sign/private.key"), StandardCharsets.US_ASCII);
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Error occured get keys", ex);
        }

        this.objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }

    @Override
    public void action() {
        try {
            sendMessage(UUID.randomUUID().toString());
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, "Error occured send data", ex);
            ex.printStackTrace();
        }
    }

    protected void sendMessage(String messageUid) throws Exception {
        LOGGER.info("Not implement sendMessage with uid:" + messageUid);
    }
}
