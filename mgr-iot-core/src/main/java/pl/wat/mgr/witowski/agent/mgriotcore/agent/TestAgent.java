package pl.wat.mgr.witowski.agent.mgriotcore.agent;

import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.SendDataBehaviour;
import pl.wat.mgr.witowski.agent.mgriotcore.behaviors.TestBehavior;

import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestAgent extends AbstrackAgent {
    private static final Logger LOGGER = Logger.getLogger(TestAgent.class.getName());

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT %1$tL] [%4$-7s] %5$s %n");
//        Logger root = Logger.getLogger(TestAgent.class.getName());
//        root.setLevel(Level.ALL);
//        for (Handler handler : root.getHandlers()) {
//            if (handler.getClass().getCanonicalName().contains("pl.wat.mgr"))
//                handler.setLevel(Level.ALL);
//        }
    }

    public TestAgent() {
        super("p8rg8t2tsl", Arrays.asList("/test", "/test1"));
    }

    @Override
    protected void setup() {
//        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
//        objectObjectHashMap.put("test", "test");
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
//        addBehaviour(new SendDataBehaviour(this, "test", objectObjectHashMap, agentUid));
        super.setup();
//        addBehaviour(new TestBehavior(this, 30000, agentUid, true));
        addBehaviour(new TestBehavior(this, 5000, agentUid, true));
    }

    @Override
    public void onMessage(String message) {
        LOGGER.info("get test data " + message);
    }

    @Override
    protected void takeDown() {
        super.takeDown();
    }
}
